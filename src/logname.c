#include <errno.h>
#include <stdio.h>
#include <unistd.h>

int usage(char *argv0) {
  printf("Usage: %s\n", argv0);
  return 1;
}

int main(int argc, char *argv[]){
  int opt;
  while ((opt = getopt(argc, argv, ":")) != -1) {
    switch (opt) {
      case '?':
        fprintf(stderr, "%s: invalid option -- '%c'\n", argv[0], optopt);
        return usage(argv[0]);
    }
  }

  if (getlogin() != NULL) {
    printf("%s\n", getlogin());
  } else {
    perror(argv[0]);
  }
  return errno;
}
