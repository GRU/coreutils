#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

int usage(char *argv0) {
  printf("Usage: %s [-u] [file ...]\n", argv0);
  return 2;
}

int main(int argc, char *argv[]){
	char ch;
  int opt;
  FILE *file = stdin;
  while ((opt = getopt(argc, argv, ":u")) != -1) {
    switch (opt) {
      case 'u':
        break;
      case '?':
        fprintf(stderr, "%s: invalid option -- '%c'\n", argv[0], optopt);
        return usage(argv[0]);
    }
  }

  if (optind == argc) { // FIXME: this code is not clean
    goto READ_STDIN;
  }

  for (; optind < argc; optind++) {
    if (strcmp(argv[optind], "-")) {
      file = fopen(argv[optind], "r");
      if (file == NULL) {
        fprintf(stderr, "%s: ", argv[0]);
        perror(argv[optind]);
        return -1;
      }
    } else {
      file = stdin;
    }
READ_STDIN:
		while ((ch = fgetc(file)) != EOF) {
			putchar(ch);
		}
    if (file != NULL && file != stdin) {
      fclose(file);
    }
  }
  return 0;
}
