#include <stdio.h>
#include <dirent.h>

// TODO: add more sorted output (. and .. in the top) and flags

int main(int argc, char *argv[]) {
  DIR *dp;
  struct dirent *ep;
  if (argc > 1) {
    dp = opendir(argv[1]);
    if (dp != NULL) {
      while ((ep = readdir (dp))) {
	printf("%s\n", ep->d_name);
      }
      closedir(dp);
    } else {
      perror("ls");
    }
    return 0;
  } else {
    dp = opendir("./");
    if (dp != NULL) {
      while ((ep = readdir (dp))) {
	printf("%s\n", ep->d_name);
      }
      closedir(dp);
    } else {
      perror("ls");
    }
    return 0;
  }
}
