#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){
  mode_t mode = 0755;
  for (int i = 1; i < argc; i++) {
    mkdir(argv[i], mode);
  }
}
